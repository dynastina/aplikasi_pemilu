<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserAccessMenu;
use Validator;

class ApiController extends Controller
{
    public function accessProcess(Request $request, UserAccessMenu $userRoleAccess)
    {
        $req = $request->all();

        $role_id = $req['role_id'];
        $menu_id = $req['menu_id'];

        if(UserAccessMenu::where('menu_id', $menu_id)->where('role_id',$role_id)->count() > 0){
            UserAccessMenu::where('menu_id', $menu_id)
            ->where('role_id',$role_id)
            ->delete();
        }else{
            UserAccessMenu::where('menu_id', $menu_id)
            ->where('role_id',$role_id)
            ->create($req);
        }
    }

    public function changeProccess(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_baru' => [
                'required',
                'string',
                'min:4',
                'max:20',
                'regex:/[a-z]/',
                'regex:/[0-9]/'
            ],
            'password_confirmation' => 'required|same:password_baru'
        ], [
            'required' => 'Data :attribute harus di isi!',
            'min' => 'Panjang :attribute minimal 4 karakter!',
            'max' => 'Panjang :attribute maksimal 20 karakter!',
            'regex' => 'Format :attribute harus mengandung huruf dan angka!',
            'same' => 'Konfirmasi Password tidak cocok dengan password baru!'
        ]);

        if ($validator->passes()) {

            return response()->json(['success'=>'Added new records.']);
			
        }

        return $data = response()->json(['error'=>$validator->errors()]);

    }
}
