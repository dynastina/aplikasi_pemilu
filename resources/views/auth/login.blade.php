@extends('layouts.main-auth')
@section('content')
	<!--begin::Login-->
		<!--begin::Content-->
		<div class="login-content flex-lg-row-fluid d-flex flex-column justify-content-center position-relative overflow-hidden py-20 px-10 p-lg-7 mx-auto mw-450px w-100">
			<!--begin::Wrapper-->
			<div class="d-flex flex-column-fluid flex-center py-10">
				<!--begin::Signin Form-->
				<form class="form w-100" method="POST" action="{{ route('login') }}">
					@csrf
					<!--begin::Title-->
					<div class="pb-5 pb-lg-15">
						<h3 class="fw-bolder text-dark display-6">Selamat datang</h3>
						<div class="text-muted fw-bold fs-3">Silahkan masukan akun anda! 
							{{-- <a href="{{ url('/register') }}" class="text-primary fw-bolder" >Daftar Akun</a> --}}
						</div>
					</div>

					@if (session('status'))
					<div class="pb-5 pb-lg-15">
						<!--begin::Alert-->
						<div class="alert alert-dismissible bg-light-primary d-flex flex-column flex-sm-row p-5 mb-10">
							

							<!--begin::Wrapper-->
							<div class="d-flex flex-column pe-0 pe-sm-10">
								<!--begin::Title-->
								<h5 class="mb-1">Sukses!</h5>
								<!--end::Title-->
								<!--begin::Content-->
								<span>{{ session('status') }}</span>
								{{-- <span>Selamat anda berhasil logout, sampai jumpa kembali!</span> --}}
								<!--end::Content-->
							</div>
							<!--end::Wrapper-->

							<!--begin::Close-->
							<button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
								<span class="svg-icon svg-icon-1 svg-icon-primary">&times;</span>
							</button>
							<!--end::Close-->
						</div>
						<!--end::Alert-->
					</div>
					@endif

					<!--end::Alert-->
					<!--begin::Title-->
					<!--begin::Form group-->
					<div class="fv-row mb-10">
						<label class="form-label fs-6 fw-bolder text-dark" for="email">Email</label>
						<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >

						@error('email')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<!--end::Form group-->
					<!--begin::Form group-->
					<div class="fv-row mb-10">
						<div class="d-flex justify-content-between mt-n5">
							<label class="form-label fs-6 fw-bolder text-dark pt-5" for="password">Password</label>
							
						</div>
						<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

						@error('password')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
						@enderror
					</div>
					<!--end::Form group-->
					<!--begin::Action-->
					<div class="pb-lg-0 pb-5">
						<button type="submit" id="kt_login_signin_form_submit_button" class="btn btn-primary fw-bolder fs-6 px-8 py-4 my-3 me-3">Sign In</button>
						
					</div>
					<!--end::Action-->
				</form>
				
			</div>
			<!--end::Wrapper-->
			<!--begin::Footer-->
			<div class="d-flex justify-content-lg-start justify-content-center align-items-center py-7 py-lg-0">
				<a href="#" class="text-primary fw-bolder fs-4">Terms</a>
				<a href="#" class="text-primary ms-10 fw-bolder fs-4">Plans</a>
				<a href="#" class="text-primary ms-10 fw-bolder fs-4">Contact Us</a>
			</div>
			<!--end::Footer-->
		</div>
		<!--end::Content-->

	<!--end::Login-->
@endsection