<div class="aside-workspace my-7 ps-5 pe-4 ps-lg-10 pe-lg-6" id="kt_aside_wordspace">
    <!--begin::Logo-->
    <div class="aside-logo py-2 pb-7" id="kt_aside_logo">
        <a href="{{ url('home') }}" class="d-flex align-items-center">
            <img alt="Logo" src="{{ URL::asset('assets/media/logos') }}/kpu.svg" class="mh-50px me-5" />
            <h3 class="text-dark fw-bolder my-1 fs-3 text-uppercase">dprd tingkat i</h3>
        </a>
    </div>
    <!--end::Logo-->
    <!--begin::Aside Menu-->
    <!--begin::Menu-->
    <div class="menu menu-column menu-rounded menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500 fw-bold fs-6" data-kt-menu="true">
        <div class="hover-scroll-y pe-4 pe-lg-5" id="kt_aside_menu_scroll" data-kt-scroll="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo" data-kt-scroll-wrappers="#kt_aside_wordspace" data-kt-scroll-offset="10px">
            <div class="menu-wrapper menu-column menu-fit sidebar-main">
                <?php   

                $user = Auth::user();
                $role_id = $user->role_id;
                $menu = DB::table('user_menus')
                                    ->select('user_menus.*')
                                    ->join('user_access_menus', 'user_menus.id','=','user_access_menus.menu_id')
                                    ->where('user_access_menus.role_id', '=', $role_id)                    
                                    ->orderBy('user_menus.order', 'asc')
                                    ->get();
                

                ?>
                @foreach($menu as $mn)
                    <div class="menu-item here show">
                        <h4 class="menu-content text-muted mb-0 fs-6 fw-bold text-uppercase">{{ $mn->name }}</h4>
                        <div class="dropdown-divider mt-0 mb-3"></div>
                        <div class="menu-sub menu-fit menu-sub-accordion show pb-10">
                            <?php 

                            $submenu = DB::table('user_sub_menus')
                                    ->where('menu_id', '=', $mn->id)
                                    ->where('is_active', '=', 1)
                                    ->orderBy('user_sub_menus.order', 'asc')
                                    ->get();
                            ?>
                            @foreach($submenu as $sm)
                            <div class="menu-item">
                                <a class="menu-link py-2" href="{{ url($sm->url) }}">
                                    <i class="{{ $sm->icon }} me-5 fs-3"></i>
                                    <span class="menu-title">{{ $sm->name }}</span>
                                </a>
                            </div>
                            @endforeach

                        </div>
                    </div>
                
                @endforeach
                
            </div>
        </div>
    </div>
    <!--end::Menu-->
</div>