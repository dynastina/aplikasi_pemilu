<!--begin::Header Search-->
<div class="modal bg-white fade" id="kt_header_search_modal" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content shadow-none">
            <div class="container w-lg-800px">
                <div class="modal-header d-flex justify-content-end border-0">
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-light-primary ms-2" data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect fill="#000000" x="0" y="7" width="16" height="2" rx="1" />
                                    <rect fill="#000000" opacity="0.5" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)" x="0" y="7" width="16" height="2" rx="1" />
                                </g>
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <div class="modal-body">
                    <!--begin::Search-->
                    <form class="pb-10">
                        <input autofocus="" type="text" class="form-control bg-transparent border-0 fs-4x text-center fw-normal search-form" name="query" placeholder="Search..." autocomplete="off"/>
                    </form>
                    <!--end::Search-->
                    <!--end::Shop Goods-->
                    @php
                        $user = Auth::user();
                        $role_id = $user->role_id;
                        $menu = DB::table('user_menus')
                                            ->select('user_menus.*')
                                            ->join('user_access_menus', 'user_menus.id','=','user_access_menus.menu_id')
                                            ->where('user_access_menus.role_id', '=', $role_id)                    
                                            ->orderBy('user_menus.order', 'asc')
                                            ->get();
                    @endphp
                    <div class="menu-search">
                        @foreach ($menu as $mn)
                            <div>
                                <h2>{{ $mn->name }}</h2>
                                @php
                                    $submenu = DB::table('user_sub_menus')
                                    ->where('menu_id', '=', $mn->id)
                                    ->where('is_active', '=', 1)
                                    ->orderBy('user_sub_menus.order', 'asc')
                                    ->get();
                                @endphp

                                @foreach ($submenu as $sm)
                                    # <a href="{{ url($sm->url) }}">{{ $sm->name }}</a>
                                @endforeach
                                <hr>
                            </div>
                        @endforeach
                        <br>
                    </div>
                    <!--begin::Tutorials-->
                    <div class="pb-10">
                        <h3 class="text-dark fw-bolder fs-1 mb-6">Tutorials</h3>
                        <!--begin::List Widget 5-->
                        <div class="card mb-5 shadow-none">
                            <!--begin::Body-->
                            <div class="card-body pt-2 px-0">
                                <!--begin::Item-->
                                <div class="d-flex mb-6">
                                    <!--begin::Icon-->
                                    <div class="me-1">
                                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Angle-right.svg-->
                                        <span class="svg-icon svg-icon-sm svg-icon-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999)" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <a href="#" class="fs-6 fw-bolder text-hover-primary text-gray-800 mb-2">Bagaimana caranya logout?</a>
                                        <div class="fw-bold text-muted">Tekan logo user dipojok kanan atas lalu klik 'Sign Out'</div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Item-->
                                <!--begin::Item-->
                                <div class="d-flex mb-6">
                                    <!--begin::Icon-->
                                    <div class="me-1">
                                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Angle-right.svg-->
                                        <span class="svg-icon svg-icon-sm svg-icon-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999)" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <a href="#" class="fs-6 fw-bolder text-hover-primary text-gray-800 mb-2">Bagaimana cara mengganti data diri?</a>
                                        <div class="fw-bold text-muted">Cari menu 'Manage Profile' lalu edit sesuai keinginan</div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Item-->
                                <!--begin::Item-->
                                <div class="d-flex mb-6">
                                    <!--begin::Icon-->
                                    <div class="me-1">
                                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Angle-right.svg-->
                                        <span class="svg-icon svg-icon-sm svg-icon-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999)" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <a href="#" class="fs-6 fw-bolder text-hover-primary text-gray-800 mb-2">Bisakah mengganti foto profile?</a>
                                        <div class="fw-bold text-muted">Bisa, buka menu manage profile dan upload foto profile anda</div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Item-->
                                <!--begin::Item-->
                                <div class="d-flex mb-6">
                                    <!--begin::Icon-->
                                    <div class="me-1">
                                        <!--begin::Svg Icon | path: icons/duotone/Navigation/Angle-right.svg-->
                                        <span class="svg-icon svg-icon-sm svg-icon-primary">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-270.000000) translate(-12.000003, -11.999999)" />
                                                </g>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                    </div>
                                    <!--end::Icon-->
                                    <!--begin::Content-->
                                    <div class="d-flex flex-column">
                                        <a href="#" class="fs-6 fw-bolder text-hover-primary text-gray-800 mb-2">Bagaimana cara menambah mengelola menu yang ada?</a>
                                        <div class="fw-bold text-muted">Pastikan anda adalah admin, lalu buka menu 'Menu Management'</div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Item-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::List Widget 5-->
                    </div>
                    <!--end::Tutorials-->
                </div>
            </div>
        </div>
    </div>
</div>
<!--end::Header Search-->

<!--begin::Delete Profile Modal-->
<div class="modal fade" tabindex="-1" id="modal_delete_profile" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content bg-light-danger">
            <div class="modal-body p-0">
                <!--begin::Alert-->
                <div class="alert alert-dismissible bg-light-danger d-flex flex-center flex-column pe-4">
                    <!--begin::Close-->
                    <button type="button" class="position-absolute top-0 end-0 m-2 btn btn-icon btn-icon-danger" data-bs-dismiss="modal" aria-label="Close">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.25" fill-rule="evenodd" clip-rule="evenodd" d="M2.36899 6.54184C2.65912 4.34504 4.34504 2.65912 6.54184 2.36899C8.05208 2.16953 9.94127 2 12 2C14.0587 2 15.9479 2.16953 17.4582 2.36899C19.655 2.65912 21.3409 4.34504 21.631 6.54184C21.8305 8.05208 22 9.94127 22 12C22 14.0587 21.8305 15.9479 21.631 17.4582C21.3409 19.655 19.655 21.3409 17.4582 21.631C15.9479 21.8305 14.0587 22 12 22C9.94127 22 8.05208 21.8305 6.54184 21.631C4.34504 21.3409 2.65912 19.655 2.36899 17.4582C2.16953 15.9479 2 14.0587 2 12C2 9.94127 2.16953 8.05208 2.36899 6.54184Z" fill="#12131A"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.29289 8.29289C8.68342 7.90237 9.31658 7.90237 9.70711 8.29289L12 10.5858L14.2929 8.29289C14.6834 7.90237 15.3166 7.90237 15.7071 8.29289C16.0976 8.68342 16.0976 9.31658 15.7071 9.70711L13.4142 12L15.7071 14.2929C16.0976 14.6834 16.0976 15.3166 15.7071 15.7071C15.3166 16.0976 14.6834 16.0976 14.2929 15.7071L12 13.4142L9.70711 15.7071C9.31658 16.0976 8.68342 16.0976 8.29289 15.7071C7.90237 15.3166 7.90237 14.6834 8.29289 14.2929L10.5858 12L8.29289 9.70711C7.90237 9.31658 7.90237 8.68342 8.29289 8.29289Z" fill="#12131A"></path>
                            </svg>
                        </span>
                    </button>
                    <!--end::Close-->

                    <!--begin::Icon-->
                    <span class="svg-icon svg-icon-5tx svg-icon-danger mb-5">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"><path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"></path>
                            <rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"></rect>
                            <rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"></rect>
                        </svg>
                    </span>
                    <!--end::Icon-->

                    <!--begin::Wrapper-->
                    <div class="text-center">
                        <!--begin::Title-->
                        <h5 class="fw-bolder fs-1 mb-5">Peringatan</h5>
                        <!--end::Title-->

                        <!--begin::Separator-->
                        <div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
                        <!--end::Separator-->

                        <!--begin::Content-->
                        <div class="mb-9">
                            Tindakan ini akan <span class="text-danger fw-boldest">menghapus</span> foto profile anda dan menggantikannya menjadi foto <strong>default</strong>.<br/>
                            <div class="d-flex flex-row align-items-center justify-content-evenly mt-5">
                                <img src="{{ asset('storage') }}/{{ Auth::user()->image }}" alt="Profile" class="mh-md-90px mh-65px">
                                <i class="fas fa-angle-double-right fs-3hx text-danger"></i>
                                <img src="{{ asset('assets/media/svg/avatars/default.svg') }}" alt="Default Profile" class="mh-md-90px mh-65px">
                            </div>
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--end::Alert-->
            </div>
            <div class="modal-footer flex-center">
                <form action="{{ url('profile/reset/'. Auth::user()->id) }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ Auth::user()->image }}" name="oldImage">
                    <!--begin::Buttons-->
                    <div class="d-flex flex-wrap">
                        <button type="button" class="btn btn-outline btn-outline-danger btn-active-danger m-2" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger m-2">Hapus Profile</button>
                    </div>
                    <!--end::Buttons-->
                </form>
            </div>
        </div>
    </div>
</div>
<!--end::Delete Profile Modal-->

<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <!--begin::Svg Icon | path: icons/duotone/Navigation/Up-2.svg-->
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <polygon points="0 0 24 0 24 24 0 24" />
                <rect fill="#000000" opacity="0.5" x="11" y="10" width="2" height="10" rx="1" />
                <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
            </g>
        </svg>
    </span>
    <!--end::Svg Icon-->
</div>
<!--end::Scrolltop-->